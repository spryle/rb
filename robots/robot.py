import constants

from utils import next, prev
from grid import Coord


class Robot(object):

    def __init__(self, x, y, orientation, grid):
        try:
            self.position = grid[int(x)][int(y)]
        except IndexError:
            raise ValueError('Initial position out of bounds.')
        self.orientation = orientation
        self.grid = grid
        self.is_lost = False

    @property
    def report(self):
        return '{position.x} {position.y} {orientation} {lost}'.format(
            position=self.position,
            orientation=self.orientation,
            lost='LOST' if self.is_lost else ''
        ).strip()

    @property
    def bleep(self):
        return 'BLEEP'

    def instructions(self, instructions):
        for instruction in instructions:
            self.instruction(instruction)

    def instruction(self, instruction):
        if instruction not in constants.INSTRUCTIONS:
            raise ValueError('Invalid instruction {0}'.format(instruction))
        if not self.is_lost:
            if instruction == constants.FORWARD:
                self.forward()
            elif instruction == constants.LEFT:
                self.left()
            elif instruction == constants.RIGHT:
                self.right()

    def left(self):
        self.orientation = prev(constants.ORIENTATIONS, self.orientation)

    def right(self):
        self.orientation = next(constants.ORIENTATIONS, self.orientation)

    def forward(self):
        try:
            if self.orientation == constants.NORTH:
                self.position = self.grid[self.position.x][self.position.y + 1]
            elif self.orientation == constants.EAST:
                self.position = self.grid[self.position.x + 1][self.position.y]
            elif self.orientation == constants.SOUTH:
                self.position = self.grid[self.position.x][self.position.y - 1]
            elif self.orientation == constants.WEST:
                self.position = self.grid[self.position.x - 1][self.position.y]
        except IndexError:
            if not self.position.scent:
                self.grid[self.position.x][self.position.y] = Coord(
                    self.position.x,
                    self.position.y,
                    True
                )
                self.is_lost = True


def build_robot(x, y, orientation, grid):
    return Robot(x, y, orientation, grid)
