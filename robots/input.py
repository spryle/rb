from utils import process_input
from grid import build_grid
from robot import build_robot


def handle_input(user_input):
    bounds, robots = process_input(user_input)
    grid = build_grid(*bounds)
    for initial, instructions in robots:
        if len(instructions) >= 100:
            raise ValueError('Instruction sequence is too long.')
        initial.append(grid)
        robot = build_robot(*initial)
        robot.instructions(instructions)
        print robot.report
