def process_input(data):
    bounds, parts = data.strip('\n').split('\n', 1)
    bounds = filter(None, bounds.split(' '))
    robots = []
    for part in parts.split('\n\n'):
        initial, instructions = part.split('\n')
        robots.append((filter(None, initial.split(' ')), instructions))
    return bounds, robots


def next(array, item):
    try:
        return array[array.index(item) + 1]
    except IndexError:
        return array[0]


def prev(array, item):
    try:
        return array[array.index(item) - 1]
    except IndexError:
        return array[len(array) - 1]
