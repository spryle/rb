from collections import namedtuple

Coord = namedtuple('Coord', ['x', 'y', 'scent'])


def build_grid(xb, yb):
    xb = int(xb)
    yb = int(yb)
    if xb > 50 or yb > 50:
        raise ValueError('Grid size too large.')
    return [[Coord(x, y, False) for y in range(yb + 1)] for x in range(xb + 1)]
