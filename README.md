## Install

install python & virtualenv
    
    virtualenv rb
    source rb/bin/activate
    cd rb
    mkdir src
    cd src
    git clone git@bitbucket.org:spryle/rb.git
    pip install -r rb/requirements.txt
    py.test
