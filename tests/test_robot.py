import pytest
from robots.robot import build_robot
from robots.grid import build_grid
from robots import constants


def test_robot():
    robot = build_robot(0, 0, constants.NORTH, build_grid(5, 5))
    assert robot.position.x == 0
    assert robot.position.y == 0
    assert robot.orientation == constants.NORTH


def test_robot_initially_out_of_bounds():
    with pytest.raises(ValueError):
        build_robot(6, 0, constants.NORTH, build_grid(5, 5))


def test_robot_invalid_instruction():
    robot = build_robot(0, 0, constants.NORTH, build_grid(5, 5))
    with pytest.raises(ValueError):
        robot.instruction('P?')


def test_robot_instructions():
    robot = build_robot(0, 0, constants.NORTH, build_grid(5, 5))
    robot.instructions([
        constants.FORWARD,
        constants.RIGHT,
        constants.FORWARD,
        constants.FORWARD,
        constants.RIGHT,
        constants.FORWARD,
        constants.LEFT,
    ])
    assert robot.position.x == 2
    assert robot.position.y == 0
    assert robot.orientation == constants.EAST


def test_robot_forward():
    robot = build_robot(0, 0, constants.NORTH, build_grid(5, 5))
    robot.instruction(constants.FORWARD)
    assert robot.position.x == 0
    assert robot.position.y == 1
    assert robot.orientation == constants.NORTH


def test_robot_left():
    robot = build_robot(0, 0, constants.NORTH, build_grid(5, 5))
    robot.instruction(constants.LEFT)
    assert robot.position.x == 0
    assert robot.position.y == 0
    assert robot.orientation == constants.WEST


def test_robot_right():
    robot = build_robot(0, 0, constants.NORTH, build_grid(5, 5))
    robot.instruction(constants.RIGHT)
    assert robot.position.x == 0
    assert robot.position.y == 0
    assert robot.orientation == constants.EAST


def test_robot_lost():
    robot = build_robot(0, 0, constants.NORTH, build_grid(1, 1))
    assert not robot.is_lost
    robot.instruction(constants.FORWARD)
    assert not robot.is_lost
    robot.instruction(constants.FORWARD)
    assert robot.is_lost


def test_robot_lost_scent():
    grid = build_grid(1, 1)
    robot = build_robot(0, 0, constants.NORTH, grid)
    assert not robot.is_lost
    robot.instruction(constants.FORWARD)
    assert not robot.is_lost
    robot.instruction(constants.FORWARD)
    assert robot.is_lost
    assert grid[0][1].scent


def test_robot_lost_scent_protection():
    grid = build_grid(1, 1)
    robot = build_robot(0, 0, constants.NORTH, grid)
    assert not robot.is_lost
    robot.instruction(constants.FORWARD)
    assert not robot.is_lost
    robot.instruction(constants.FORWARD)
    assert robot.is_lost
    assert grid[0][1].scent
    another_robot = build_robot(0, 1, constants.NORTH, grid)
    another_robot.instruction(constants.FORWARD)
    assert not another_robot.is_lost
    assert another_robot.position.x == 0
    assert another_robot.position.y == 1
