import pytest

from robots.grid import build_grid


def test_grid():
    grid = build_grid(5, 5)
    assert len(grid) == 6
    assert len(grid[0]) == 6
    return True


def test_grid_too_large():
    with pytest.raises(ValueError):
        build_grid(55, 35)
    with pytest.raises(ValueError):
        build_grid(30, 60)
