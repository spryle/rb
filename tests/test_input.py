from robots.input import handle_input

INPUT = '''
5 3
1 1 E
RFRFRFRF

3 2 N
FRRFLLFFRRFLL

0 3 W
LLFFFLFLFL
'''

OUTPUT = '''
1 1 E
3 3 N LOST
2 3 S
'''


def test_challenge_input(capsys):
    handle_input(INPUT)
    out, err = capsys.readouterr()
    assert out.strip('\n') == OUTPUT.strip('\n')
